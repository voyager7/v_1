How to run this application:
1. Download and install Neo4j Community edition
2. Start Neo4j server and verify its running by visiting http://localhost:7474
3. If you have STS (Spring Tool Suite) plugin installed in eclipse
	- Right click on the project, select Run As, then select spring boot app.
	This will start up the application and have it running on port 8080. To verify the
	rest service is up and running, try visiting http://locahost:8080 on a browser and
	you see a Landing page Text. An example with curl from command line is shown below:
	curl -XGET localhost:8080
	- If you dont eclipse STS plugin, right click on project, go to Run As, select maven clean.
	Then right click on project again, go to Run As, select maven install. To run the app, right
	on the project one more time, select Run As, then Java Application. When prompted to choose
	a main class, select project.Application. Then you can test it as shown above.
	
4. Currently the only implemented flow is the /register, the mailing part is configured but
	not yet ready for use. To play with the registration endpoint, play around with the following
	curl commands
	curl -X POST -d '{}' localhost:8080/register --header "Content-Type:application/json"
	- Use this to add a user, if user was added successfully, it should not return anything
	curl -X POST -d '{"age":"16","username":"testUsername","email":"testEmail@email.com","first_name":"fname","last_name":"lname"}' localhost:8080/api/register --header "Content-Type:application/json"
	- Try repeating the above request to add another user with the same username, this should throw a 409 conflict error
	- Try misspelling a key in the data and check the results, example, change username to usernames as shown below:
	curl -X POST -d '{"age":"16","usernames":"testUsername","email":"testEmail@email.com","first_name":"fname","last_name":"lname"}' localhost:8080/api/register --header "Content-Type:application/json"

//NB: If you were able to successfully add a new user, you can view the node details by going to http://localhost:7474, then clicking the three round bubbles
on the left side bar, then clicking User button under Node labels. This should display all users stored in DB on the right hand side. Clicking a bubble
in the right pane will display more information or data about the node.
	
	
	
//TODO:
	- Add email and account verification support
	- Configure user controller
	- Beef up the registration endpoint to handle other errors and validations
	- Configure adding trips and also modifying how node entities are indexed and stored in DB
	
	
	
	
