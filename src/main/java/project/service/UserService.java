package main.java.project.service;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import main.java.project.Utils.ConstantUtil;
import main.java.project.Utils.StringUtil;
import main.java.project.Utils.Util;
import main.java.project.domain.User;
import main.java.project.exception.Error;
import main.java.project.mail.SmtpMailSender;
import main.java.project.repository.UserRepository;

import org.neo4j.graphdb.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.neo4j.core.GraphDatabase;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class UserService {
	
	@Autowired
    UserRepository userRepository;
	
	@Autowired
	GraphDatabase graphDatabase;
	
	@Resource
	SmtpMailSender smtpMailSender;
	
	private static final Logger logger = LoggerFactory.getLogger(UserService.class);
	
	public User registerNewUser(HttpServletRequest request, Map<String, String> payload) throws Error{
		String requestId = ((String)request.getAttribute(ConstantUtil.REQUEST_ID)).toString();
		
		String username = payload.get(ConstantUtil.USERNAME);
		String age = payload.get(ConstantUtil.AGE);
		String email = payload.get(ConstantUtil.EMAIL);
		String first_name = payload.get(ConstantUtil.FIRST_NAME);
		String last_name = payload.get(ConstantUtil.LAST_NAME);
		
		// Validate User input is neither empty nor missing
		StringBuilder exceptionReason = new StringBuilder();
		StringUtil.assertNotNullAndEmptyForException(ConstantUtil.USERNAME, username, exceptionReason);
		StringUtil.assertNotNullAndEmptyForException(ConstantUtil.AGE, age, exceptionReason);
		StringUtil.assertNotNullAndEmptyForException(ConstantUtil.EMAIL, email, exceptionReason);
		StringUtil.assertNotNullAndEmptyForException(ConstantUtil.FIRST_NAME, first_name, exceptionReason);
		StringUtil.assertNotNullAndEmptyForException(ConstantUtil.LAST_NAME, last_name, exceptionReason);
		if(!exceptionReason.toString().isEmpty()){
			logger.error(String.format("registration exception occured. req id : [%s]", requestId));
			Error ex = new Error(String.format("the following missing fields are required for user"
					+ " registration [ %s ]", exceptionReason.toString()));
			ex.setQuick_help(String.format("verify you are passing the following fields in your POST request [ %s ], also validate "
					+ "none of the field(s) are empty. For additional help visit the help_url link.", exceptionReason.toString()));
			
			ex.setCode("missing_required_data");
			ex.setHelp_url(ConstantUtil.HELP_URL);
			ex.setRequest_id(requestId);
			ex.setHttpStatus(HttpStatus.BAD_REQUEST);
			ex.setType("error");
			throw ex;
		}
		
		// Validate username is available for registration
		User existingUser = userRepository.findByUsername(username);
		if(existingUser != null){
			Error ex = new Error(String.format("username unvailable for registration [ %s ]", username));
			ex.setQuick_help(String.format("username [ %s ] cannot be used, provide another username", username));
			ex.setCode("username_unavailable");
			ex.setHelp_url(ConstantUtil.HELP_URL);
			ex.setRequest_id(requestId);
			ex.setHttpStatus(HttpStatus.CONFLICT);
			ex.setType("error");
			throw ex;			
		}
		
		// Create new user
		User newUser = new User();
		newUser.setUsername(username);
		newUser.setAge(age);
		newUser.setEmail(email);
		newUser.setFirst_name(first_name);
		newUser.setLast_name(last_name);
		newUser.setUser_id(Util.getUniqueId());

		//Use transaction to insert user into Graph DB
		Transaction tx = graphDatabase.beginTx();
		try{
			userRepository.save(newUser);
			tx.success();
			//smtpMailSender.send("xxx@gmail.com", "hello world", "this is a test");
			logger.debug(String.format("successfully saved new user: [ %s ]", newUser));
		}catch(Exception ex){
			logger.error(String.format("Exception Occured while saving new User to DB : [usermame: %s, user_id: %s]"
					, newUser.getUsername(), newUser.getUser_id()) + ex);
			Error dbEx = new Error("Error occured while saving user to database");
			dbEx.setQuick_help("retry your request again");
			dbEx.setCode("service_unavailable");
			dbEx.setHelp_url(ConstantUtil.HELP_URL);
			dbEx.setRequest_id(requestId);
			dbEx.setHttpStatus(HttpStatus.SERVICE_UNAVAILABLE);
			dbEx.setType("error");
			throw dbEx;
		}finally{
			tx.close();
		}
		return newUser;
	}
}
