package main.java.project.repository;

import main.java.project.domain.User;

import org.springframework.data.neo4j.repository.GraphRepository;

public interface UserRepository extends GraphRepository<User>{
	User findByUsername(String username);
}
