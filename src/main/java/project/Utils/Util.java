package main.java.project.Utils;

import java.util.UUID;

public class Util {
	public static String getUniqueId(){
		return UUID.randomUUID().toString().replaceAll("-", "");
	}
}
