package main.java.project.Utils;

public class StringUtil {
	public static void assertNotNullAndEmptyForException(String name, String content, StringBuilder outputString){
		if(content == null || content.isEmpty()){
			String stringBuilderContent = outputString.toString();
			if(stringBuilderContent.isEmpty()){
				outputString.append(name);
			}
			else{
				outputString.append(" , "+ name);
			}
		}
	}
}
