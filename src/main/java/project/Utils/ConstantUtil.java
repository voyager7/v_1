package main.java.project.Utils;

public class ConstantUtil {
	public final static String REQUEST_ID = "request_id";
	
	// Ajax registration form constants for new user
	public final static String USERNAME = "username";
	public final static String FIRST_NAME = "first_name";
	public final static String LAST_NAME = "last_name";
	public final static String AGE = "age";
	public final static String EMAIL = "email";
	
	// Help url for exceptions
	public final static String HELP_URL = "http://localhost:8080/help";
}
