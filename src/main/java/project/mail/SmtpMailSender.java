package main.java.project.mail;

import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;

@Configuration
@PropertySource("classpath:mail.properties")
public class SmtpMailSender {
	
	private static final Logger logger = LoggerFactory.getLogger(SmtpMailSender.class);
	
    @Value("${mail.protocol}")
    private String protocol;
    @Value("${mail.host}")
    private String host;
    @Value("${mail.port}")
    private int port;
    @Value("${mail.smtp.auth}")
    private boolean auth;
    @Value("${mail.smtp.starttls.enable}")
    private boolean starttls;
    @Value("${mail.from}")
    private String from;
    @Value("${mail.username}")
    private String username;
    @Value("${mail.password}")
    private String password;
	
	@Autowired
	private JavaMailSender javaMailSender;
	
    @Bean
    public JavaMailSender javaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        Properties mailProperties = new Properties();
        mailProperties.put("mail.smtp.auth", auth);
        mailProperties.put("mail.smtp.starttls.enable", starttls);
        mailSender.setJavaMailProperties(mailProperties);
        mailSender.setHost(host);
        mailSender.setPort(port);
        mailSender.setProtocol(protocol);
        mailSender.setUsername(username);
        mailSender.setPassword(password);
        return mailSender;
    }
	
    @Async
	public void send(String to, String subject, String body) throws MessagingException{
    	logger.debug(String.format("Sending confirmation email to [ %s ]", to));
		MimeMessage message = javaMailSender.createMimeMessage();
		MimeMessageHelper msgHelper = new MimeMessageHelper(message, true); // true for multipart message
		msgHelper.setSubject(subject);
		msgHelper.setTo(to);
		msgHelper.setText(body, true); // true indicates html
		javaMailSender.send(message);
		logger.debug(String.format("Confirmation email successfully sent to [ %s ]", to));
	}
}
