package main.java.project.domain;

import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.NodeEntity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@NodeEntity
@JsonInclude(Include.NON_NULL)
public class Trip {
	private @GraphId Long id;
	private String user_id;
	private String trip_name;
	private String declared_start_date;
	private String declared_end_date;
	private String destination;
	/**
	 * @return the user_id
	 */
	public String getUser_id() {
		return user_id;
	}
	/**
	 * @param user_id the user_id to set
	 */
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	/**
	 * @return the trip_name
	 */
	public String getTrip_name() {
		return trip_name;
	}
	/**
	 * @param trip_name the trip_name to set
	 */
	public void setTrip_name(String trip_name) {
		this.trip_name = trip_name;
	}
	/**
	 * @return the declared_start_date
	 */
	public String getDeclared_start_date() {
		return declared_start_date;
	}
	/**
	 * @param declared_start_date the declared_start_date to set
	 */
	public void setDeclared_start_date(String declared_start_date) {
		this.declared_start_date = declared_start_date;
	}
	/**
	 * @return the decalred_end_date
	 */
	public String getDeclared_end_date() {
		return declared_end_date;
	}
	/**
	 * @param decalred_end_date the decalred_end_date to set
	 */
	public void setDeclared_end_date(String declared_end_date) {
		this.declared_end_date = declared_end_date;
	}
	/**
	 * @return the destination
	 */
	public String getDestination() {
		return destination;
	}
	/**
	 * @param destination the destination to set
	 */
	public void setDestination(String destination) {
		this.destination = destination;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("user_id: ").append(user_id).append("\n")
		.append("trip_name: ").append(trip_name).append("\n")
		.append("declared_start_date: ").append(declared_start_date).append("\n")
		.append("declared_end_date: ").append(declared_end_date).append("\n")
		.append("destination: ").append(destination).append("\n");
		return sb.toString();
	}
	
}
