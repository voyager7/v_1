package main.java.project.domain;

import java.util.HashSet;
import java.util.Set;

import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.NodeEntity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@NodeEntity
@JsonInclude(Include.NON_NULL)
public class User {
	private @GraphId Long id;
	@JsonIgnore
	private String user_id;
	private String username;
	private String first_name;
	private String last_name;
	private String age;
	private String email;
	private Set<Trip> trips;
	
	public User(){
		trips = new HashSet<Trip>();
	}
	
	/**
	 * @return the user_id
	 */
	public String getUser_id() {
		return user_id;
	}
	
	/**
	 * @param user_id the user_id to set
	 */
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the first_name
	 */
	public String getFirst_name() {
		return first_name;
	}
	
	/**
	 * @param first_name the first_name to set
	 */
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	
	/**
	 * @return the last_name
	 */
	public String getLast_name() {
		return last_name;
	}
	
	/**
	 * @param last_name the last_name to set
	 */
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	
	/**
	 * @return the age
	 */
	public String getAge() {
		return age;
	}
	
	/**
	 * @param age the age to set
	 */
	public void setAge(String age) {
		this.age = age;
	}
	
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
	/**
	 * @return the trips
	 */
	public Set<Trip> getTrips() {
		return trips;
	}
	
	/**
	 * @param trips the trips to set
	 */
	public void setTrips(Set<Trip> trips) {
		this.trips = trips;
	}
	
	/**
	 * @param trips adds a new trip for user
	 */
	public void addTrip(Trip trips) {
		this.trips.add(trips);
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("user_id: ").append(user_id).append("\n")
		.append("username: ").append(username).append("\n")
		.append("first_name: ").append(first_name).append("\n")
		.append("last_name: ").append(last_name).append("\n")
		.append("age: ").append(age).append("\n")
		.append("email: ").append(email).append("\n").append("trips: ").append("\n\t");
		
		for(Trip trip : trips){
			sb.append(trip.toString()).append("\n\t");
		}
		return sb.toString();
	}
}
