package main.java.project.exception;

//200	success
//201	created
//202	accepted
//204	no_content
//302	redirect
//304	not_modified
//400	bad_request
//401	unauthorized
//403	forbidden
//404	not_found
//405	method_not_allowed
//409	conflict
//412	precondition_failed
//429     too_many_requests
//500	internal_server_error
//503     unavailable
public class JSONException {
	private String type;
	private String status;
	private String code;
	private String help_url;
	private String request_id;
	private String reason;
	private String quick_help;
	
	/**
	 * @return the quick_help
	 */
	public String getQuick_help() {
		return quick_help;
	}

	/**
	 * @param quick_help the quick_help to set
	 */
	public void setQuick_help(String quick_help) {
		this.quick_help = quick_help;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	/**
	 * @return the reason
	 */
	public String getReason() {
		return reason;
	}

	/**
	 * @param reason the reason to set
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}
	
	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}
	
	/**
	 * @return the help_url
	 */
	public String getHelp_url() {
		return help_url;
	}
	
	/**
	 * @param help_url the help_url to set
	 */
	public void setHelp_url(String help_url) {
		this.help_url = help_url;
	}
	
	/**
	 * @return the request_id
	 */
	public String getRequest_id() {
		return request_id;
	}
	
	/**
	 * @param request_id the request_id to set
	 */
	public void setRequest_id(String request_id) {
		this.request_id = request_id;
	}
}
