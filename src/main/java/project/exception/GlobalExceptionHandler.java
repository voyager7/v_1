package main.java.project.exception;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import main.java.project.Utils.ConstantUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

//200	success
//201	created
//202	accepted
//204	no_content
//302	redirect
//304	not_modified
//400	bad_request
//401	unauthorized
//403	forbidden
//404	not_found
//405	method_not_allowed
//409	conflict
//412	precondition_failed
//429     too_many_requests
//500	internal_server_error
//503     unavailable

@ControllerAdvice
public class GlobalExceptionHandler{
	private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);
	
	@ExceptionHandler(Error.class)
	public @ResponseBody JSONException handleException(HttpServletRequest request, HttpServletResponse response, Error ex){
		String requestId = ((String)request.getAttribute(ConstantUtil.REQUEST_ID)).toString();
		logger.error(String.format("Registration Exception occured. Req ID : [%s]", requestId));
		JSONException exception = new JSONException();
		exception.setCode(ex.getCode());
		exception.setHelp_url(ex.getHelp_url());
		exception.setRequest_id(ex.getRequest_id());
		exception.setStatus(ex.getStatus());
		exception.setType(ex.getType());
		exception.setReason(ex.getMessage());
		exception.setQuick_help(ex.getQuick_help());
		response.setStatus(ex.getHttpStatus().value());
		return exception;
	}

}
