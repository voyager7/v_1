package main.java.project.exception;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class Error extends Exception{
	/**
	 * Default serial version
	 */
	private static final long serialVersionUID = 1L;
	private String type;
	private String status;
	private String code;
	private String help_url;
	private String request_id;
	private String quick_help;
	@JsonIgnore
	private HttpStatus httpStatus;
	
	public Error(String message){
		super(message);
	}
	
	public Error(String message, String quick_resolution){
		super(message);
		this.quick_help = quick_resolution;
	}

	/**
	 * @return the httpStatus
	 */
	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

	/**
	 * @param httpStatus the httpStatus to set
	 */
	public void setHttpStatus(HttpStatus httpStatus) {
		this.status = httpStatus.toString();
		this.httpStatus = httpStatus;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the help_url
	 */
	public String getHelp_url() {
		return help_url;
	}

	/**
	 * @param help_url the help_url to set
	 */
	public void setHelp_url(String help_url) {
		this.help_url = help_url;
	}

	/**
	 * @return the request_id
	 */
	public String getRequest_id() {
		return request_id;
	}

	/**
	 * @param request_id the request_id to set
	 */
	public void setRequest_id(String request_id) {
		this.request_id = request_id;
	}

	/**
	 * @return the quick_tip
	 */
	public String getQuick_help() {
		return quick_help;
	}

	/**
	 * @param quick_tip the quick_tip to set
	 */
	public void setQuick_help(String quick_help) {
		this.quick_help = quick_help;
	}
}
