package main.java.project.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import main.java.project.Utils.ConstantUtil;
import main.java.project.Utils.Util;
import main.java.project.domain.User;
import main.java.project.exception.Error;
import main.java.project.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * Registration Controller to register new users
 *
 */
@RestController
public class RegistrationController {
    
	@Autowired
	UserService userService;
	
    @RequestMapping(value="/register",  method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE,
    		produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public HttpHeaders register(@RequestBody Map<String, String> registrationPayload, HttpServletRequest request) throws Error {
    	request.setAttribute(ConstantUtil.REQUEST_ID, Util.getUniqueId());
    	User newUser = userService.registerNewUser(request, registrationPayload);
    	HttpHeaders headers = new HttpHeaders();
    	headers.setLocation(linkTo(UserController.class).slash(newUser.getUsername()).toUri());
    	return headers;
    }
    
    
}
