package main.java.project.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RootController {

    @RequestMapping(value="/",  method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public String register(HttpServletRequest request){
    	return "\nLanding Page for Travel Application Rest Service Endpoint\n";
    }
	
}
